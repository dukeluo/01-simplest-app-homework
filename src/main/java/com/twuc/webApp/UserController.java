package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @GetMapping("/api/tables/plus")
    public String getAdditionTable() {
        return new MathOperationTable("add").createTable();
    }

    @GetMapping("/api/tables/multiply")
    public String getMultiplicationTable() {
        return new MathOperationTable("mul").createTable();
    }
}
