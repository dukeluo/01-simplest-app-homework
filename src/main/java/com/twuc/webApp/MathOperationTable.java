package com.twuc.webApp;

public class MathOperationTable {
    private OperatorFunction operator;
    private String operatorSymbol;
    private String operatorString;

    public MathOperationTable(String operatorString) {
        this.operatorString = operatorString;
        switch (operatorString) {
            case "add":
                this.operator = (x, y) -> x + y;
                this.operatorSymbol = "+";
                break;
            case "mul":
                this.operator = (x, y) -> x * y;
                this.operatorSymbol = "*";
                break;
        }
    }

    public String createTable() {
        StringBuilder builder = new StringBuilder();

        for (int i = 1; i < 10; i++) {
            for (int j = 1; j <= i; j++) {
                builder.append(i)
                        .append(this.operatorSymbol)
                        .append(j)
                        .append("=")
                        .append(this.operator.operator(i, j));
                if (j != i) {
                    builder.append(" ");
                    if (this.operator.operator(i, j) < 10) {
                        builder.append((" "));
                    }
                }
                if (this.operatorString.equals("mul") && j == 1 && i != 1) {
                    builder.deleteCharAt(builder.length()-1);
                }
            }
            builder.append("\n");
        }
        return builder.toString();
    }
}

@FunctionalInterface
interface OperatorFunction {
    int operator(int x, int y);
}

